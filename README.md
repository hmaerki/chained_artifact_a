# chained_artifact_A

The `test` stage is executed only when `TRIGGER_TEST` is defined when starting the pipeline.

`DOWNSTREAM_PARAM` will be passed to the downstream when `DOWNSTREAM_PARAM` is defined when manually starting the pipeline.

## Downstream will be triggered when

- Variable `TRIGGER_DOWNSTREAM` is not `skip` when manually starting the pipeline
- Commit message contains `downstream`.

## Various

- https://docs.gitlab.com/ee/ci/yaml/#yaml-anchors-for-variables
If your commit message contains `[ci skip]` the commit will be created but the pipeline will be skipped
